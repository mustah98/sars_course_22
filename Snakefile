import os
import sys
import importlib
import pandas as pd

configfile: "config.yaml"
path_abs = os.path.abspath(os.getcwd())
print(path_abs)
input = config["data"]
print("input: " + str(input))

files = sorted(list(os.listdir(input)))
sample_names = [name.split(".")[0] for name in files][::2]

R1 = list([input + name for name in files if "R1" in name])
R2 = list([input + name for name in files if "R2" in name])

sample_table = {
	"index" :   sample_names,
	"R1"    :   R1,
	"R2"    :   R2,
}

sample_table = pd.DataFrame.from_dict(data = sample_table)
sample_table = sample_table.set_index("index")

def mapping(mapper):
	if mapper == "bwa":
		return ("results/mapping/{sample}_bwa.sam")
	elif mapper == "minimap":
		return ("results/mapping/{sample}_minimap.sam")

def ruleAll():

	if config["mode"] == "wgs":
		if config["qc"] == "t":
			print("mode: wgs with quality controll")
			return(
				expand("results/trimm/{sample}_trimm_1.fastq.gz", sample = sample_names),
				expand("results/trimm/{sample}_trimm_2.fastq.gz", sample = sample_names),
				expand("results/qc/{sample}_trimm_1_fastqc.html", sample = sample_names),
				expand("results/qc/{sample}_trimm_2_fastqc.html", sample = sample_names),
				expand("results/output_stats/{sample}.txt", sample = sample_names),
				expand("results/output_stats/{sample}_pileup.txt", sample = sample_names),
				expand("results/consensus/{sample}_consensus.fasta", sample = sample_names),
				expand("results/output_stats/{sample}_coverage_plot.png", sample = sample_names),
				)
		elif config["qc"] == "f":
			print("mode: wgs with no quality controll")
			return(
				expand("results/trimm/{sample}_trimm_1.fastq.gz", sample = sample_names),
				expand("results/trimm/{sample}_trimm_2.fastq.gz", sample = sample_names),
				expand("results/output_stats/{sample}.txt", sample = sample_names),
				expand("results/output_stats/{sample}_pileup.txt", sample = sample_names),
				expand("results/consensus/{sample}_consensus.fasta", sample = sample_names),
				expand("results/output_stats/{sample}_coverage_plot.png", sample = sample_names),
				)
	if config["mode"] == "cp":
		if config["qc"] == "t":
			print("mode: capture with quality controll")
			return(
				expand("results/trimm/{sample}_trimm_1.fastq.gz", sample = sample_names),
				expand("results/trimm/{sample}_trimm_2.fastq.gz", sample = sample_names),
				expand("results/qc/{sample}_trimm_1_fastqc.html", sample = sample_names),
				expand("results/qc/{sample}_trimm_2_fastqc.html", sample = sample_names),
				expand("results/output_stats/{sample}.txt", sample = sample_names),
				expand("results/output_stats/{sample}_pileup.txt", sample = sample_names),
				expand("results/consensus/{sample}_consensus.fasta", sample = sample_names),
				expand("results/output_stats/{sample}_coverage_plot.png", sample = sample_names),
				)
		elif config["qc"] == "f":
			print("mode: capture with no quality controll")
			return(
				expand("results/trimm/{sample}_trimm_1.fastq.gz", sample = sample_names),
				expand("results/trimm/{sample}_trimm_2.fastq.gz", sample = sample_names),
				expand("results/output_stats/{sample}.txt", sample = sample_names),
				expand("results/output_stats/{sample}_pileup.txt", sample = sample_names),
				expand("results/consensus/{sample}_consensus.fasta", sample = sample_names),
				expand("results/output_stats/{sample}_coverage_plot.png", sample = sample_names),
				)

rule all:
	input:
		ruleAll()

rule trimm:
	input:
		R1 = lambda wildcards: sample_table.at[wildcards.sample, "R1"] if wildcards.sample in sample_table.index else "",
		R2 = lambda wildcards: sample_table.at[wildcards.sample, "R2"] if wildcards.sample in sample_table.index else "",
	output:
		R1 = "results/trimm/{sample}_trimm_1.fastq.gz",
		R2 = "results/trimm/{sample}_trimm_2.fastq.gz",
		HTML = "results/trimm/{sample}.html",
		JSON = "results/trimm/{sample}.json",
	shell:
		"fastp -i {input.R1} -I {input.R2} -o {output.R1} -O {output.R2}  -h results/trimm/{wildcards.sample}.html -j  results/trimm/{wildcards.sample}.json --qualified_quality_phred 20 --length_required 50"

rule qc:
	input:
		R1 = "results/trimm/{sample}_trimm_1.fastq.gz",
		R2 = "results/trimm/{sample}_trimm_2.fastq.gz",
	output:
		OUT = "results/qc/{sample}_trimm_1_fastqc.html",
		OUT2 = "results/qc/{sample}_trimm_2_fastqc.html",
	shell:
		"fastqc {input.R1} {input.R2} -o results/qc/"


rule mapping_bwa:
	input:
		R1 = "results/trimm/{sample}_trimm_1.fastq.gz",
		R2 = "results/trimm/{sample}_trimm_2.fastq.gz",
		REF = config["ref"]
	output:
		SAM = "results/mapping/{sample}_bwa.sam",
	shell:
		"""
		bwa index {input.REF}
		bwa mem {input.REF} {input.R1} {input.R2} > {output.SAM}
		"""


rule mapping_minimap:
	input:
		R1 = "results/trimm/{sample}_trimm_1.fastq.gz",
		R2 = "results/trimm/{sample}_trimm_2.fastq.gz",
		REF = config["ref"]
	output:
		SAM = "results/mapping/{sample}_minimap.sam"
	shell:
		"minimap2 -x sr -a -o {output.SAM} {input.REF} {input.R1} {input.R2}"


rule samtools:
	input:
		SAM = mapping(config["map"])
	output:
		BAM1 = "results/samtools/{sample}.bam",
		BAM2 = "results/samtools/{sample}_sorted.bam",
		BAM3 = "results/samtools/{sample}_sorted.bam.bai",
	shell:
		"""
		samtools view -bS {input.SAM} -o {output.BAM1}
		samtools sort {output.BAM1} -o {output.BAM2}
		samtools index {output.BAM2} >  {output.BAM3}
		"""

rule varriant_calling_wgs:
	input:
		BAM = "results/samtools/{sample}_sorted.bam",
	output:
		VCF = "results/varriant_calling/{sample}.vcf",
	params:
		REF = config["ref"],
	shell:
		"freebayes -f {params.REF} --min-alternate-count 10 --min-alternate-fraction 0.1 --min-coverage 20 --pooled-continuous --haplotype-length -1 {input.BAM} > {output.VCF}"

rule zip:
	input:
		VCF = "results/varriant_calling/{sample}.vcf",
	output:
		ZIP = "results/varriant_calling/{sample}.vcf.gz",
	shell:
		"""
		bgzip -f {input.VCF}
		tabix -f -p vcf {output.ZIP}
		"""

rule consensus:
	input:
		ZIP = "results/varriant_calling/{sample}.vcf.gz",
		REF = config["ref"],
	output:
		FASTA = "results/consensus/{sample}_consensus.fasta",
	shell:
		"bcftools consensus -f {input.REF} {input.ZIP} -o {output.FASTA}"

rule samtools_flagstats:
	input:
		BAM = "results/samtools/{sample}_sorted.bam",
	output:
		STATS = "results/output_stats/{sample}.txt"
	shell:
		"samtools flagstats {input.BAM} > {output.STATS} "

rule samtools_pileup:
	input:
		BAM = "results/samtools/{sample}_sorted.bam",
	output:
		PILUP = "results/output_stats/{sample}_pileup.txt"
	shell:
		"samtools mpileup {input.BAM} > {output.PILUP}"

rule vis:
	input: "results/output_stats/{sample}_pileup.txt",
	output:
		OUT = "results/output_stats/{sample}_coverage_plot.png",
	run:
		path = os.path.abspath(os.getcwd()) + "/" + input[0]
		data = pd.read_csv(path , delimiter="\t")
		data = data.iloc[:,[1,3]]
		data.columns = ["position", "depth"]
		out = path_abs + "/" + output.OUT
		plot4 = data.plot(x="position", y=["depth"]).get_figure().savefig(out)
