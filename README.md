# SARS-CoV-2 genome assembly from Illumina reads

###### Practical course: SARS-2 Bioinformatics & Data Science (19406213)

## Project Description
Carry out a SARS-2 genome assembly from illumina sequencing data derived from WGS and capture sequencing.
With the help of our pipeline it is possible to run the assembly using only a reference for mapping and paired end samples of your interest.

## Requirements
To make an assembly you need the input data including a reference genome and a paired end sample in .fasta format and enviroment files for runing the pipeline. The pipeline is written in the workflowmanagement system snakemake, where you need conda to execute it.

### Installation
#### via Conda
First install conda via [conda](https://conda.io/projects/conda/en/latest/user-guide/install/index.html).

To install all dependencies necessary for the pipeline conda will create an enviroment based on the yaml file in envs/ by running:

```bash
conda env create -f env/environment.yaml
```

Then activate environment by running:
```bash
conda activate assemblPipe
```
And install snakemake:

```bash
conda install snakemake
```

## Configuration
The pipeline can be configured directly via the command line. It allows you to make your workflow more flexible as changing the reference genome and which sequencing methods your sample is coming from. All the options are defined in the python executable pipe.py.

Parameters:
- -t/--threads: set number of threads to use
- -i/--input: path to directory of input files
- -q/--qc: quality control boolean / t or f
- -m/--mode: sequencing technology used for sequences. <wg> for WGS / <cp> for CAPTURE
- -r/--reference: path to the reference genome

## Usage
Once the pipeline is configured it can be executed via python. Before executing, the reference file must be indexed using minimap indexing if minimap2 is configured, or using bwa indexing if bwa is configured as the mapper of choice. Then execute the pipeline as shown:

```bash
python3 pipe.py -i <path/to/data/> -m <cp/wgs> -q <t/f> -r <path/to/ref> -a <bwa/multimap>
```

Note: It is required that the samples in the input directory are from the same mode!

## Test Pipe using testdata
The Pipeline can be tested after setting it up with the following command:
```bash
python pipe.py -i test_pipeline_data/sequencecs/ -r test_pipeline_data/reference/NC_045512.2.fasta -m cp -q t -a bwa
```

## Results
A results directory is created automaticly using snakemake. All results are safed according to it's process.

## Note
When using amplicon sequences, too many variables need to be taken into account. To provided an automated pipeline without complications, we decided to remove the support for amplicon sequences.

## Contributers and acknowledgment
Feel free to consult us for questions or feedback:

Ashkan Ghassemi @ashkag98 <ashkag98(at)zedat.fu-berlin.de>

Kevin Zidane @kikoucha <kevin.zidane(at)fu-berlin.de>

Mustafa Helal @mustah98 <mustah98(at)zedat.fu-berlin.de>
