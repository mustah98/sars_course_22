import os
import sys
import argparse
import yaml


parser = argparse.ArgumentParser()
parser.add_argument("-t", "--threads", help = "set amount of threads to use")
parser.add_argument("-i", "--input", help = "path to dir of input files")
parser.add_argument("-q", "--qc", help = "quality controll boolean / t or f")
parser.add_argument("-m", "--mode", help = "sequencing technology used for sequences. <wg> for WGS / <amp> for AMPLICON / <cp> for CAPTURE")
parser.add_argument("-r", "--reference", help = "path to the reference genome")
parser.add_argument("-a", "--mapper", help = "chose mapper to do analysis with. Options: -a bwa or -a minimap")


path_Snakefile = os.path.abspath(os.getcwd()) + "/Snakefile"
print("..............")
print(path_Snakefile)
args = parser.parse_args()

if args.threads:
    print("threads: % s" % args.threads)
    threads = args.threads
else:
    threads = 4
    print("threads: % s" % threads)


if args.mode:
    if args.mode in ["wgs","cp"]:
        print("mode: %s" % args.mode)
        mode = str(args.mode)
    else:
        sys.exit("Error: Defined sequening method is wrong specified or wasnt specified")

if args.input:
    if os.path.exists(args.input):
        print("input: % s" % args.input)
        input = str(args.input)
    else:
        sys.exit("Error: Path to file directorz=y does not exist or was not provided")


if args.qc:
    if args.qc == "t":
        print("qc: %s" % args.qc)
        qc = str(args.qc)
    else:
        qc = "f"
        print("qc: %s" % qc)


if args.reference:
    print("reference: %s" % args.reference)
    ref = str(args.reference)
else:
    sys.exit("Error: reference genome was wrongly or not specified")


if args.mapper:
    mapper = str(args.mapper)
    if mapper == "bwa" or mapper == "minimap":
        mapper = mapper
        print("mapper: %s" % mapper)
    else:
        sys.exit("Error: No mapper was choosen or wrong one was specified. Choose between <bwa> or <minimap>.")

stream = open("config.yaml", 'r')
data = yaml.safe_load(stream)

data["data"] = input
data["mode"] = mode
data["qc"] = qc
data["ref"] = ref
data["map"] = mapper


with open("config.yaml", 'w') as yaml_file:
    yaml_file.write( yaml.dump(data, default_flow_style=False))


if sys.argv[1] != "--help" or sys.argv[1] != "-h":
    os.system("snakemake --configfile=config.yaml --use-conda --cores " + str(threads))
